FROM ubuntu:focal AS base

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update -qq && \
	apt-get install -qq -y --no-install-recommends xsltproc docbook-xsl nant && \
	apt-get install -qq -y --no-install-recommends aspell aspell-en && \
	apt-get install -qq -y --no-install-recommends git patch && \
	apt-get clean && rm -rf /var/lib/apt/lists/*

CMD ["bash"]

FROM base

RUN apt-get update -qq && \
	apt-get install -qq -y --no-install-recommends fop fonts-freefont-ttf && \
	apt-get clean && rm -rf /var/lib/apt/lists/*
